# Created for use with Python 3.4 and later
# Copyright (C) 2021, Jason Guyer

from tkinter import *
from tkinter import filedialog as fd
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox
import glob
import shutil
import os
import time
import threading
import math


class MainApplication:
	#Class variables
	lblPath = None
	entPath = None
	btnPath = None
	lblLinesPerFile = None
	entLinesPerFile = None
	btnSplitFile = None
	lblStatus = None
	lblLinesInFile = None
	txtLinesInFile = None
	lblFilesAfterSplit = None
	txtFilesAfterSplit = None
	lblProgress = None
	pgsSplitProgress = None
	TotalLines = None
	Filename = None
	Filename_no_ext = None
	File_Extension = None
	OpenFile = None
	TotalLines = None
	LinesPerFile = None
	WorkingDirectory = None
	BackupDirectory = None
	NumberOfFilesCreated = None
	
	
	def __init__(self):
		self.top = Tk()
		self.top.title('File Splitter')
		self.top.resizable(0, 0)
		self.top.option_add('*Font', 'Times 10')
		self.menubar = Menu(self.top)
		self.menu_about = Menu(self.menubar, tearoff=0)
		self.lblPath = ttk.Label(self.top, text='File Path')
		self.entPath = Entry(self.top, width=50)
		self.btnPath = ttk.Button(self.top, text='...', command=self.open_file)
		self.lblLinesPerFile = ttk.Label(self.top, text='# of lines per file (blank defaults to 1,000,000)')
		self.entLinesPerFile = Entry(self.top)
		self.entLinesPerFile.bind("<Key>", self.callback)
		self.lblLinesInFile = ttk.Label(self.top, text='Number of lines in opened file')
		self.txtLinesInFile = Text(self.top, height=1, width=10, background='#f0f0f0', state=DISABLED, borderwidth=0, relief='solid')
		self.lblFilesAfterSplit = ttk.Label(self.top, text='Number of files after the split')
		self.txtFilesAfterSplit = Text(self.top, height=1, width=10, background='#f0f0f0', state=DISABLED, borderwidth=0, relief='solid')
		self.separate_me = ttk.Separator(self.top, orient=HORIZONTAL)
		self.btnSplitFile = ttk.Button(self.top, text='SPLIT FILE', command=self.threaded_backup_file, state=DISABLED)
		self.lblStatus = ttk.Label(self.top, text='Status: IDLE')
		self.lblProgress = ttk.Label(self.top, text='Splitting Progress')
		self.pgsSplitProgress = ttk.Progressbar(self.top, orient=HORIZONTAL, length=100, mode='determinate')
		
	def build_ui(self):
		self.menubar.add_cascade(label='Help', menu=self.menu_about)
		self.menu_about.add_command(label='About...', command=self.about_window)
		self.top.config(menu=self.menubar)
		self.lblPath.grid(row=0, column=0, sticky='W')
		self.entPath.grid(row=1, column=0, sticky='EW')
		self.btnPath.grid(row=1, column=1, sticky='EW')
		self.lblLinesPerFile.grid(row=2, column=0, sticky='W')
		self.entLinesPerFile.grid(row=2, column=1, sticky='EW')
		self.lblLinesInFile.grid(row=3, column=0, sticky='W')
		self.txtLinesInFile.grid(row=3, column=1, sticky='EW')
		self.lblFilesAfterSplit.grid(row=4, column=0, sticky='W')
		self.txtFilesAfterSplit.grid(row=4, column=1, sticky='EW')
		self.separate_me.grid(row=5, columnspan=2, sticky='EW')
		self.lblProgress.grid(row=6, columnspan=2, sticky='W')
		self.pgsSplitProgress.grid(row=7, columnspan=3, sticky='EW')
		self.lblStatus.grid(row=8, column=0, sticky='W')
		self.btnSplitFile.grid(row=0, column=2, rowspan=7, sticky='NS')
		
	
	def split_file(self):
		line_count = 0
		file_count = 1
		lines_per_file = None
		if self.entLinesPerFile.get() == '':
			lines_per_file = 1000000
		else:
			lines_per_file = int(self.entLinesPerFile.get())
		# print('Lines per file: ' + str(lines_per_file))
		# f = open(self.OpenFile, 'r')
		# lines = f.readlines()
		# print('Lines have been read')
		# f.close()				
		newfile = self.WorkingDirectory + '\\' + self.Filename_no_ext + str(file_count).zfill(2) + self.File_Extension
		# print('New file: ' + newfile)
		f = open(newfile, 'w')
		with open(self.OpenFile) as FileObj:
			for line in FileObj:
			# for line in lines:
				if line_count < lines_per_file:
					f.write(line)
					line_count = line_count+1
				else:
					f.close()
					line_count = 0
					file_count = file_count +1
					self.pgsSplitProgress['value'] = (file_count / self.NumberOfFilesCreated)*100
					newfile = self.WorkingDirectory + '\\' + self.Filename_no_ext + str(file_count).zfill(2) + self.File_Extension
					f = open(newfile, 'w')
			f.close()
		self.ui_change_idle()
		self.clear_after_process()
		
		
	def backup_file(self):
		self.btnSplitFile.config(state=DISABLED)
		backup_dir = self.WorkingDirectory + '\\Backup\\'
		if not os.path.exists(backup_dir):
				os.makedirs(backup_dir)
		else:
			filelist = glob.glob(backup_dir+'*.*')
			for filepath in filelist:
				try:
					os.remove(filepath)
				except:
					None
		backup_path = backup_dir + self.Filename
		shutil.copyfile(self.OpenFile, backup_path)
		self.threaded_split_file()
		
		
	def open_file(self):
		filetypes = [
			('Text Files', '*.txt'),
			('CSV Files', '*.csv'),
			('DAT Files', '*.dat'),
			('Log Files', '*.log'),
			('All Files', '*.*')]
		self.OpenFile = fd.askopenfilename(title='Open a file', filetypes=filetypes)
		if self.OpenFile == '':
			None
		else:
			self.entPath.delete(0, 'end')
			self.entPath.insert(0, self.OpenFile)
			self.Filename = os.path.basename(self.OpenFile)
			self.Filename_no_ext = os.path.splitext(self.Filename)[0]
			self.File_Extension = os.path.splitext(self.Filename)[1]
			self.WorkingDirectory = os.path.dirname(os.path.abspath(self.OpenFile))
			self.threaded_get_lines()
			
			
	def clear_after_process(self):
		os.remove(self.OpenFile)
		self.entPath.delete(0, 'end')
		self.entLinesPerFile.delete(0, 'end')
		self.txtLinesInFile.config(state=NORMAL)
		self.txtLinesInFile.delete('1.0', END)
		self.txtLinesInFile.config(state=DISABLED)
		self.txtFilesAfterSplit.config(state=NORMAL)
		self.txtFilesAfterSplit.delete('1.0', END)
		self.txtFilesAfterSplit.config(state=DISABLED)
		self.TotalLines = None
		self.Filename = None
		self.Filename_no_ext = None
		self.File_Extension = None
		self.OpenFile = None
		self.TotalLines = None
		self.LinesPerFile = None
		self.WorkingDirectory = None
		self.BackupDirectory = None
		self.NumberOfFilesCreated = None
		self.pgsSplitProgress['value'] = 0
		
		
	def threaded_backup_file(self):
		UIThread = threading.Thread(target=self.ui_change_backup_file)
		UIThread.start()
		DataThread = threading.Thread(target=self.backup_file)
		DataThread.start()
		
		
	def threaded_get_lines(self):
		UIThread = threading.Thread(target=self.ui_change_open_file)
		UIThread.start()
		DataThread = threading.Thread(target=self.get_all_lines)
		DataThread.start()
		
		
	def threaded_split_file(self):
		UIThread = threading.Thread(target=self.ui_change_split_file)
		UIThread.start()
		DataThread = threading.Thread(target=self.split_file)
		DataThread.start()
		
	
	def get_all_lines(self):
		count = 0
		with open(self.OpenFile) as FileObj:
			for lines in FileObj:
				count = count+1
		self.TotalLines = count
		self.lblStatus.config(text='Status: IDLE')
		self.btnSplitFile.config(state=NORMAL)
		# f.close()
		self.txtLinesInFile.config(state=NORMAL)
		self.txtLinesInFile.delete('1.0', END)
		self.txtLinesInFile.insert(INSERT, str(self.TotalLines))
		self.txtLinesInFile.config(state=DISABLED)
		self.get_files_after_split()
		
		
	def get_files_after_split(self):
		split_point = None
		if self.entLinesPerFile.get() == '':
			split_point = '1000000'
		else:
			split_point = self.entLinesPerFile.get()
		# print('Split Point: ' + str(split_point))
		if split_point.isnumeric():
			if self.TotalLines == None:
				None
			else:
				self.NumberOfFilesCreated = math.ceil(self.TotalLines / int(split_point))
				self.txtFilesAfterSplit.config(state=NORMAL)
				self.txtFilesAfterSplit.delete('1.0', END)
				self.txtFilesAfterSplit.insert(INSERT, str(self.NumberOfFilesCreated))
				self.txtFilesAfterSplit.config(state=DISABLED)
		else:
			tkinter.messagebox.showerror('Non-Numeric Input','Please enter numbers only in the number of lines per file field.')
			self.entLinesPerFile.delete(0, 'end')
		
		
	def callback(self, event):
		self.top.after(1, self.get_files_after_split)
		
		
	def ui_change_open_file(self):
		self.lblStatus.config(text='Status: Analyzing file')
		
		
	def ui_change_backup_file(self):
		self.lblStatus.config(text='Status: Backing up the original file')
		
		
	def ui_change_split_file(self):
		self.lblStatus.config(text='Status: Splitting the file')
		
		
	def ui_change_idle(self):
		self.lblStatus.config(text='Status: IDLE')
		
		
	# Give credit where credit is due
	def about_window(self):
		me = AboutMe()
		me.build_ui()
		
		
class AboutMe:
	# class variables
	win = None
	lblTitle = None
	lblAbout = None
	lblAuthor = None
	btnClose = None
	
	def __init__(self):
		self.win = tk.Toplevel()
		self.lblTitle = ttk.Label(self.win, text='Text File Splitter', font='Times 18', anchor=CENTER, justify=CENTER)
		self.lblAbout = ttk.Label(self.win, text='A tool to split large text files into small\nfiles that can be opened by a text editor', justify=CENTER)
		self.lblAuthor = ttk.Label(self.win, text='By Jason Guyer\n©2023 All Rights Reserved')
		self.btnClose = ttk.Button(self.win, text='Close', command=self.close_about)
		self.build_ui
		
		
	def build_ui(self):
		self.win.wm_title('About')
		self.win.resizable(0, 0)
		self.lblTitle.grid(row=0, column=0, padx=8, pady=(8, 0), sticky='EW')
		self.lblAbout.grid(row=1, column=0, padx=8, pady=(0, 8), sticky='NSEW')
		self.lblAuthor.grid(row=2, column=0, padx=8, pady=8, sticky='W')
		self.btnClose.grid(row=3, column=0, sticky='EW')
		
		
	def close_about(self):
		self.win.destroy()
		


if __name__ == '__main__':
	# Loop the loop!
	root = MainApplication()
	root.build_ui()
	root.top.mainloop()
