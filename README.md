# Text file splitter

If you are having problems opening a text based file (.txt, .csv, .log, etc.) then this is the application for you! With this you can dynamically see how many files there will be after the split and retain the original file in a backup directory.

Click the elipsis in the window and select the file and wait for the file to be scanned. Change the number of lines per file in the split files (default is 1,000,000) and click the button to split it.
